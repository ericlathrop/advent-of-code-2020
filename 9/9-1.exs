defmodule AdventOfCode do
  def check(numbers) do
    preamble_size = 25
    window_size = preamble_size + 1

    numbers =
      numbers
      |> Enum.map(&String.to_integer/1)

    0..(Enum.count(numbers) - window_size)
    |> Enum.map(&Enum.slice(numbers, &1, window_size))
    |> Enum.find_value(&find_invalid_number/1)
  end

  defp find_invalid_number(numbers) do
    [target | tail] = Enum.reverse(numbers)

    if target in all_sums(tail) do
      false
    else
      target
    end
  end

  defp all_sums([_x]), do: []

  defp all_sums([head | tail]) do
    tail
    |> Enum.map(fn n -> n + head end)
    |> Enum.concat(all_sums(tail))
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
