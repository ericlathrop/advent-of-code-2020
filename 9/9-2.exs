defmodule AdventOfCode do
  def check(numbers) do
    preamble_size = 25
    window_size = preamble_size + 1

    numbers =
      numbers
      |> Enum.map(&String.to_integer/1)

    invalid_number =
      0..(Enum.count(numbers) - window_size)
      |> Enum.map(&Enum.slice(numbers, &1, window_size))
      |> Enum.find_value(&find_invalid_number/1)
      |> IO.inspect()

    contiguous_numbers = find_contiguous_sum(numbers, invalid_number)
    min = Enum.min(contiguous_numbers)
    max = Enum.max(contiguous_numbers)
    min + max
  end

  defp find_contiguous_sum(numbers, target) do
    0..(Enum.count(numbers) - 2)
    |> Enum.map(fn start -> find_contiguous_sum(numbers, target, start) end)
    |> Enum.find_value(fn
      nil -> false
      numbers -> numbers
    end)
  end

  defp find_contiguous_sum(numbers, target, start) do
    (start + 1)..(Enum.count(numbers) - 1)
    |> IO.inspect()
    |> Enum.map(fn last -> Enum.slice(numbers, start..last) end)
    |> Enum.find_value(fn slice ->
      sum = Enum.reduce(slice, &Kernel.+/2)

      case sum do
        ^target -> slice
        _ -> false
      end
    end)
  end

  defp find_invalid_number(numbers) do
    [target | tail] = Enum.reverse(numbers)

    if target in all_sums(tail) do
      false
    else
      target
    end
  end

  defp all_sums([_x]), do: []

  defp all_sums([head | tail]) do
    tail
    |> Enum.map(fn n -> n + head end)
    |> Enum.concat(all_sums(tail))
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
