data =
  File.read!("input.txt")
  |> String.split("\n", trim: true)
  |> Enum.map(&String.to_integer/1)

# IO.inspect(data)

defmodule AdventOfCode do
  def search([head | tail]) do
    case Enum.find(tail, &(&1 + head == 2020)) do
      nil -> search(tail)
      y -> y * head
    end
  end
end

AdventOfCode.search(data)
|> IO.inspect()
