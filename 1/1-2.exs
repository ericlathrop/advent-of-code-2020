data =
  File.read!("input.txt")
  |> String.split("\n", trim: true)
  |> Enum.map(&String.to_integer/1)

# IO.inspect(data)

defmodule AdventOfCode do
  def search(_list, 0, 0), do: 1
  def search([], _target, _depth), do: nil
  def search(_list, _target, 0), do: nil

  def search([head | tail], target, depth) do
    case search(tail, target - head, depth - 1) do
      nil -> search(tail, target, depth)
      val -> val * head
    end
  end
end

AdventOfCode.search(data, 2020, 3)
|> IO.inspect()
