defmodule AdventOfCode do
  def check(boarding_passes) do
    boarding_passes
    |> Enum.map(fn boarding_pass ->
      coordinate(128, 8, boarding_pass)
      |> seat_id
    end)
    |> Enum.sort()
    |> find_gap()
  end

  defp find_gap([head | tail]), do: find_gap(tail, head)

  defp find_gap([head | tail], a) do
    if a + 2 == head do
      a + 1
    else
      find_gap(tail, head)
    end
  end

  defp coordinate(rows, cols, instructions) do
    {col_search, row_search} =
      instructions
      |> String.replace("L", "F")
      |> String.replace("R", "B")
      |> String.split_at(7)

    row = bsp_search(0, rows - 1, String.to_charlist(col_search))
    col = bsp_search(0, cols - 1, String.to_charlist(row_search))

    {row, col}
  end

  defp seat_id({row, col}) do
    row * 8 + col
  end

  def bsp_search(min, min, []), do: min

  def bsp_search(min, max, [?F | tail]) do
    range = max - min + 1
    half_range = div(range, 2)

    bsp_search(min, min + half_range - 1, tail)
  end

  def bsp_search(min, max, [?B | tail]) do
    range = max - min + 1
    half_range = div(range, 2)

    bsp_search(min + half_range, max, tail)
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
