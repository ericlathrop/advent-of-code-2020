data =
  File.read!("input.txt")
  |> String.split("\n", trim: true)
  |> Enum.map(fn line ->
    [_, first, second, char, password] = Regex.run(~r/(\d+)-(\d+) ([a-z]): ([a-z]+)/, line)

    {String.to_integer(first) - 1, String.to_integer(second) - 1, char, password}
  end)

# IO.inspect(data)

defmodule AdventOfCode do
  def match?(target, target, target), do: false
  def match?(target, target, _), do: true
  def match?(target, _, target), do: true
  def match?(_, _, _), do: false

  def check(data) do
    data
    |> Enum.count(fn {first, second, char, password} ->
      match?(char, String.at(password, first), String.at(password, second))
    end)
  end
end

AdventOfCode.check(data)
|> IO.inspect()
