data =
  File.read!("input.txt")
  |> String.split("\n", trim: true)
  |> Enum.map(fn line ->
    [_, min, max, char, password] = Regex.run(~r/(\d+)-(\d+) ([a-z]): ([a-z]+)/, line)

    {String.to_integer(min), String.to_integer(max), char, password}
  end)

# IO.inspect(data)

defmodule AdventOfCode do
  def count(char, password) do
    [c | _] = to_charlist(char)
    password = to_charlist(password)

    password
    |> to_charlist
    |> Enum.count(fn
      ^c -> true
      _ -> false
    end)
  end

  def satisfies_count?(count, min, max) when count >= min and count <= max, do: true
  def satisfies_count?(_count, _min, _max), do: false

  def check(data) do
    data
    |> Enum.count(fn {min, max, char, password} ->
      satisfies_count?(count(char, password), min, max)
    end)
  end
end

AdventOfCode.check(data)
|> IO.inspect()
