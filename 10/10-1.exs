defmodule AdventOfCode do
  def check(numbers) do
    [max | tail] =
      ["0" | numbers]
      |> Enum.map(&String.to_integer/1)
      |> Enum.sort()
      |> Enum.reverse()

    [head | tail] =
      [max + 3 | [max | tail]]
      |> Enum.reverse()

    {_, diffs} =
      Enum.reduce(tail, {head, []}, fn n, {prev, list} ->
        {n, [n - prev | list]}
      end)

    counts =
      diffs
      |> Enum.group_by(fn x -> x end)

    Enum.count(counts[1]) * Enum.count(counts[3])
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
