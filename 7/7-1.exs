defmodule AdventOfCode do
  def check(rules) do
    rules
    |> Enum.map(&parse_rule/1)
    |> Enum.flat_map(&expand_and_invert_rule/1)
    |> Enum.group_by(fn {inner, _outer} -> inner end, fn {_inner, outer} -> outer end)
    |> count_outer_bags("shiny gold")
  end

  defp parse_rule(rule) do
    [_, outer, inner] = Regex.run(~r/^(\w+ \w+) bags contain (.+).$/, rule)

    inner =
      inner
      |> String.split(", ")
      |> Enum.map(&parse_inner/1)

    {outer, inner}
  end

  defp parse_inner("no other bags"), do: {0, "other"}

  defp parse_inner(inner) do
    [_, count, color] = Regex.run(~r/^(\d+) (\w+ \w+) bag[s]?$/, inner)
    {String.to_integer(count), color}
  end

  defp expand_and_invert_rule({_outer, []}), do: []

  defp expand_and_invert_rule({outer, [{_count, inner} | tail]}) do
    [{inner, outer} | expand_and_invert_rule({outer, tail})]
  end

  defp count_outer_bags(rules, starting_color) do
    count_outer_bags(rules, rules[starting_color], list_to_map(rules[starting_color]))
    |> Enum.count()
  end

  defp list_to_map(list) do
    list
    |> Enum.map(fn item -> {item, 1} end)
    |> Map.new()
  end

  defp count_outer_bags(_rules, [], found), do: found

  defp count_outer_bags(rules, [head | tail], found) do
    colors = rules[head]

    case colors do
      nil ->
        count_outer_bags(rules, tail, found)

      colors ->
        new_colors =
          colors
          |> Enum.reject(fn color -> Map.has_key?(found, color) end)

        new_found = Map.merge(found, list_to_map(new_colors))
        count_outer_bags(rules, tail ++ new_colors, new_found)
    end
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
