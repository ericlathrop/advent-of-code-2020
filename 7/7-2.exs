defmodule AdventOfCode do
  def check(rules) do
    rules
    |> Enum.map(&parse_rule/1)
    |> Map.new()
    |> count_bags({1, "shiny gold"})
    |> Kernel.-(1)
  end

  defp parse_rule(rule) do
    [_, outer, inner] = Regex.run(~r/^(\w+ \w+) bags contain (.+).$/, rule)

    inner =
      inner
      |> String.split(", ")
      |> Enum.map(&parse_inner/1)

    {outer, inner}
  end

  defp parse_inner("no other bags"), do: {0, "other"}

  defp parse_inner(inner) do
    [_, count, color] = Regex.run(~r/^(\d+) (\w+ \w+) bag[s]?$/, inner)
    {String.to_integer(count), color}
  end

  defp count_bags(rules, starting_color) when is_binary(starting_color) do
    count_bags(rules, rules[starting_color])
  end

  defp count_bags(_rules, {0, _color}), do: 0

  defp count_bags(rules, {count, color}) do
    sum =
      rules[color]
      |> Enum.map(fn rule -> count_bags(rules, rule) end)
      |> Enum.reduce(&Kernel.+/2)

    count + count * sum
  end
end

# File.read!("sample2.txt")
# |> String.split("\n", trim: true)
# |> AdventOfCode.check()
# |> IO.inspect()

# File.read!("sample.txt")
# |> String.split("\n", trim: true)
# |> AdventOfCode.check()
# |> IO.inspect()

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
