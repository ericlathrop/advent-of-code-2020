defmodule AdventOfCode do
  def check(customs_forms) do
    customs_forms
    |> Enum.map(&check_group/1)
    |> Enum.reduce(&Kernel.+/2)
  end

  defp check_group(group) do
    people =
      String.split(group, "\n", trim: true)
      |> IO.inspect()

    num_people =
      Enum.count(people)
      |> IO.inspect()

    people
    |> Enum.map(fn questions ->
      questions
      |> String.to_charlist()
      |> Enum.map(&{&1, 1})
      |> Map.new()
    end)
    |> IO.inspect()
    |> Enum.reduce(fn x, acc ->
      Map.merge(x, acc, fn _key, a, b -> a + b end)
    end)
    |> Enum.count(fn
      {_char, ^num_people} -> true
      _ -> false
    end)
  end
end

File.read!("input.txt")
|> String.split("\n\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
