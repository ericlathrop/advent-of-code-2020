defmodule AdventOfCode do
  def check(customs_forms) do
    customs_forms
    |> Enum.map(&check_group/1)
    |> Enum.reduce(&Kernel.+/2)
  end

  defp check_group(group) do
    group
    |> String.replace("\n", "")
    |> String.to_charlist()
    |> Enum.sort()
    |> Enum.dedup()
    |> Enum.count()
  end
end

File.read!("input.txt")
|> String.split("\n\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
