defmodule AdventOfCode do
  def check(instructions) do
    instructions
    |> Enum.map(&parse_instruction/1)
    |> simulate({0, 0, %{}})
  end

  defp parse_instruction(instruction) do
    [operation, argument] =
      instruction
      |> String.split(" ")

    {operation, String.to_integer(argument)}
  end

  defp simulate(instructions, state = {instruction_pointer, accumulator, executed}) do
    if executed[instruction_pointer] do
      accumulator
    else
      instruction = Enum.at(instructions, instruction_pointer)
      # IO.puts("#{inspect(instruction)} #{inspect(state)}")
      next_state = run(instruction, state)
      simulate(instructions, next_state)
    end
  end

  defp run({"nop", _argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + 1, accumulator, Map.put(executed, instruction_pointer, true)}
  end

  defp run({"acc", argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + 1, accumulator + argument,
     Map.put(executed, instruction_pointer, true)}
  end

  defp run({"jmp", argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + argument, accumulator, Map.put(executed, instruction_pointer, true)}
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
