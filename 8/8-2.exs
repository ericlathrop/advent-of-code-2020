defmodule AdventOfCode do
  def check(instructions) do
    parsed_instructions =
      instructions
      |> Enum.map(&parse_instruction/1)

    0..(Enum.count(parsed_instructions) - 1)
    |> Enum.map(&fix_instruction(&1, parsed_instructions))
    |> Enum.reject(&(&1 == nil))
    |> Enum.find_value(fn instructions ->
      case simulate(instructions, {0, 0, %{}}) do
        {:halt, accumulator} -> {:halt, accumulator}
        _ -> false
      end
    end)
  end

  defp fix_instruction(index, instructions) do
    instruction =
      Enum.at(instructions, index)
      |> swap_instruction

    case instruction do
      nil -> nil
      instruction -> List.replace_at(instructions, index, instruction)
    end
  end

  defp swap_instruction({"jmp", argument}), do: {"nop", argument}
  defp swap_instruction({"nop", argument}), do: {"jmp", argument}
  defp swap_instruction(instruction), do: nil

  defp parse_instruction(instruction) do
    [operation, argument] =
      instruction
      |> String.split(" ")

    {operation, String.to_integer(argument)}
  end

  defp simulate(instructions, state = {instruction_pointer, accumulator, executed}) do
    cond do
      executed[instruction_pointer] ->
        {:infinite_loop, accumulator}

      instruction_pointer == Enum.count(instructions) ->
        {:halt, accumulator}

      true ->
        instruction = Enum.at(instructions, instruction_pointer)
        # IO.puts("#{inspect(instruction)} #{inspect(state)}")
        next_state = run(instruction, state)
        simulate(instructions, next_state)
    end
  end

  defp run({"nop", _argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + 1, accumulator, Map.put(executed, instruction_pointer, true)}
  end

  defp run({"acc", argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + 1, accumulator + argument,
     Map.put(executed, instruction_pointer, true)}
  end

  defp run({"jmp", argument}, {instruction_pointer, accumulator, executed}) do
    {instruction_pointer + argument, accumulator, Map.put(executed, instruction_pointer, true)}
  end
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check()
|> IO.inspect()
