defmodule AdventOfCode do
  def check(data, over, down, pos, trees) do
    data
    |> Enum.drop(down)
    |> check_over(over, down, pos, trees)
  end

  defp check_over([], _over, _down, _pos, trees), do: trees

  defp check_over(next_data, over, down, pos, trees) do
    [row | _] = next_data
    cols = String.length(row)

    next_pos = rem(pos + over, cols)

    count =
      row
      |> String.at(next_pos)
      |> count_trees

    check(next_data, over, down, next_pos, count + trees)
  end

  defp count_trees("."), do: 0
  defp count_trees("#"), do: 1
end

data =
  File.read!("input.txt")
  |> String.split("\n", trim: true)

[{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}]
|> Enum.map(fn {over, down} -> AdventOfCode.check(data, over, down, 0, 0) end)
|> Enum.reduce(&Kernel.*/2)
|> IO.inspect()
