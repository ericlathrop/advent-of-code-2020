defmodule AdventOfCode do
  def check(data, over, down, pos, trees) do
    data
    |> Enum.drop(down)
    |> check_over(over, down, pos, trees)
  end

  defp check_over([], _over, _down, _pos, trees), do: trees

  defp check_over(next_data, over, down, pos, trees) do
    [row | _] = next_data
    cols = String.length(row)

    next_pos = rem(pos + over, cols)

    count =
      row
      |> String.at(next_pos)
      |> count_trees

    check(next_data, over, down, next_pos, count + trees)
  end

  defp count_trees("."), do: 0
  defp count_trees("#"), do: 1
end

File.read!("input.txt")
|> String.split("\n", trim: true)
|> AdventOfCode.check(3, 1, 0, 0)
|> IO.inspect()
