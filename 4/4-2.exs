defmodule AdventOfCode do
  def check(passports) do
    passports
    |> Enum.map(&passport_to_map/1)
    |> Enum.count(&valid_passport?/1)
  end

  defp passport_to_map(passport) when is_binary(passport) do
    passport
    |> String.split(~r/[ \n]/, trim: true)
    |> Enum.map(fn
      s ->
        [field, value] = String.split(s, ":")
        {field, value}
    end)
    |> Map.new()
  end

  defp valid_passport?(
         %{
           "byr" => _,
           "iyr" => _,
           "eyr" => _,
           "hgt" => _,
           "hcl" => _,
           "ecl" => _,
           "pid" => _
         } = passport
       ) do
    passport
    |> Enum.all?(&is_valid_field?/1)
  end

  defp valid_passport?(_passport), do: false

  defp is_valid_field?({"byr", byr}) do
    with {n, ""} <- Integer.parse(byr) do
      n >= 1920 && n <= 2002
    else
      _ -> false
    end
  end

  defp is_valid_field?({"iyr", iyr}) do
    with {n, ""} <- Integer.parse(iyr) do
      n >= 2010 && n <= 2020
    else
      _ -> false
    end
  end

  defp is_valid_field?({"eyr", eyr}) do
    with {n, ""} <- Integer.parse(eyr) do
      n >= 2020 && n <= 2030
    else
      _ -> false
    end
  end

  defp is_valid_field?({"hgt", hgt}) do
    with {n, units} <- Integer.parse(hgt) do
      is_valid_height?(n, units)
    else
      _ -> false
    end
  end

  defp is_valid_field?({"hcl", hcl}) do
    Regex.match?(~r/^#[0-9a-f]{6}$/, hcl)
  end

  defp is_valid_field?({"ecl", "amb"}), do: true
  defp is_valid_field?({"ecl", "blu"}), do: true
  defp is_valid_field?({"ecl", "brn"}), do: true
  defp is_valid_field?({"ecl", "gry"}), do: true
  defp is_valid_field?({"ecl", "grn"}), do: true
  defp is_valid_field?({"ecl", "hzl"}), do: true
  defp is_valid_field?({"ecl", "oth"}), do: true
  defp is_valid_field?({"ecl", _}), do: false

  defp is_valid_field?({"pid", pid}) do
    Regex.match?(~r/^[0-9]{9}$/, pid)
  end

  defp is_valid_field?(_), do: true

  defp is_valid_height?(n, "cm") when n >= 150 and n <= 193, do: true
  defp is_valid_height?(n, "in") when n >= 59 and n <= 76, do: true
  defp is_valid_height?(_n, _units), do: false
end

File.read!("input.txt")
|> String.split("\n\n")
|> AdventOfCode.check()
|> IO.inspect()
