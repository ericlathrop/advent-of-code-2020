defmodule AdventOfCode do
  def check(passports) do
    passports
    |> Enum.map(&passport_to_map/1)
    |> Enum.count(&valid_passport?/1)
  end

  defp passport_to_map(passport) when is_binary(passport) do
    passport
    |> String.split(~r/[ \n]/, trim: true)
    |> Enum.map(fn
      s ->
        [field, value] = String.split(s, ":")
        {field, value}
    end)
    |> Map.new()
  end

  defp valid_passport?(%{
         "byr" => _,
         "iyr" => _,
         "eyr" => _,
         "hgt" => _,
         "hcl" => _,
         "ecl" => _,
         "pid" => _
       }),
       do: true

  defp valid_passport?(_passport), do: false
end

File.read!("input.txt")
|> String.split("\n\n")
|> AdventOfCode.check()
|> IO.inspect()
